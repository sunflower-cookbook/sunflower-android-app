package com.develop.dubhad.sunflower.settings

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.utils.PrefsManager
import com.develop.dubhad.sunflower.utils.di.Injectable
import javax.inject.Inject

class SettingsFragment : PreferenceFragmentCompat(), Injectable {

    @Inject
    lateinit var prefsManager: PrefsManager

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        val prefGestureControl =
            findPreference<SwitchPreferenceCompat>(getString(R.string.pref_gesture_control))
        val prefVoiceControl =
            findPreference<SwitchPreferenceCompat>(getString(R.string.pref_voice_control))
        prefGestureControl?.setOnPreferenceChangeListener { _, newValue ->
            prefsManager.prefGestureControl = newValue as Boolean
            true
        }
        prefVoiceControl?.setOnPreferenceChangeListener { _, newValue ->
            prefsManager.prefVoiceControl = newValue as Boolean
            true
        }
    }
}
