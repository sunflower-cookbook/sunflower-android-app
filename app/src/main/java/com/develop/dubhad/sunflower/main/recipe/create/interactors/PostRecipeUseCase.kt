package com.develop.dubhad.sunflower.main.recipe.create.interactors

import com.develop.dubhad.sunflower.main.recipe.base.models.RecipeDetail
import com.develop.dubhad.sunflower.main.recipe.base.repositories.RecipeRepository
import io.reactivex.Completable
import javax.inject.Inject

class PostRecipeUseCase @Inject constructor(private val recipeRepository: RecipeRepository) {

    fun execute(recipe: RecipeDetail): Completable {
        return recipeRepository.postRecipe(recipe)
    }
}
