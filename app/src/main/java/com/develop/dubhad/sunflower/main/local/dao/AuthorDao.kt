package com.develop.dubhad.sunflower.main.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.develop.dubhad.sunflower.main.local.models.AuthorEntity
import io.reactivex.Completable

@Dao
interface AuthorDao {
    @Query("SELECT * FROM AuthorEntity WHERE id == :authorId")
    fun getAuthor(authorId: Int): AuthorEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAuthor(authorEntity: AuthorEntity): Completable
}
