package com.develop.dubhad.sunflower.main.base.ui

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.NavArgumentBuilder
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.get
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.authentication.ui.AuthenticationActivity
import com.develop.dubhad.sunflower.utils.PrefsManager
import com.develop.dubhad.sunflower.utils.TokenManager
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var tokenManager: TokenManager

    @Inject
    lateinit var prefsManager: PrefsManager

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        navController = findNavController(R.id.nav_fragment)

        val graph = navController.navInflater.inflate(R.navigation.nav_main)
        val arg = NavArgumentBuilder().apply {
            defaultValue = getString(R.string.recipes_toolbar_label)
        }
            .build()

        if (!tokenManager.isTokenExists()) {
            graph.startDestination = R.id.recipe_list_fragment
            navigation_view.menu.removeItem(R.id.cookbook_list_fragment)
        }

        graph[R.id.recipe_list_fragment].addArgument("title", arg)
        navController.graph = graph

        appBarConfiguration = AppBarConfiguration(navController.graph, drawer_layout)

        setSupportActionBar(toolbar)
        setupActionBarWithNavController(navController, appBarConfiguration)

        navigation_view.setupWithNavController(navController)

        if (tokenManager.isTokenExists()) {
            log_in_button.visibility = View.GONE
            log_out_button.visibility = View.VISIBLE
        } else {
            log_in_button.visibility = View.VISIBLE
            log_out_button.visibility = View.GONE
        }

        log_out_button.setOnClickListener {
            prefsManager.prefSkipAuth = false
            tokenManager.clearToken()
            openAuthActivity()
        }

        log_in_button.setOnClickListener {
            openAuthActivity()
        }

        Dexter.withContext(this)
            .withPermission(Manifest.permission.RECORD_AUDIO)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                }
                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                }
                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                }
            })
            .check()

        navigation_view.setNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.local_recipe_list_fragment -> {
                    val args = Bundle().apply {
                        putBoolean("local", true)
                        putString("title", getString(R.string.saved_toolbar_label))
                    }
                    navController.navigate(R.id.recipe_list_fragment, args)
                }
            }
            drawer_layout.closeDrawer(GravityCompat.START)
            item.onNavDestinationSelected(navController)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    private fun openAuthActivity() {
        val intent = Intent(this, AuthenticationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}
