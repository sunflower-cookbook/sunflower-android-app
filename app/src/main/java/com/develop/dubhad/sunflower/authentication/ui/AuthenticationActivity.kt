package com.develop.dubhad.sunflower.authentication.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.develop.dubhad.sunflower.R
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class AuthenticationActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_authentication)
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector
}
