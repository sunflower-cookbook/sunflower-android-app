package com.develop.dubhad.sunflower.main.recipe.base.models.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RecipeStepResponse(
    @Json(name = "recipe_step_id") val recipeStepId: Int?,
    @Json(name = "description") val description: String?,
    @Json(name = "photo") val photo: String?,
    @Json(name = "step_number") val stepNumber: Int?
)
