package com.develop.dubhad.sunflower.authentication.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.authentication.interactors.LogInUseCase
import com.develop.dubhad.sunflower.authentication.models.domain.LogInUser
import com.develop.dubhad.sunflower.base.models.Resource
import com.develop.dubhad.sunflower.base.viewmodels.BaseViewModel
import com.develop.dubhad.sunflower.utils.StringsManager
import com.develop.dubhad.sunflower.utils.extensions.subscribeWithResource
import io.reactivex.rxkotlin.addTo
import retrofit2.HttpException
import java.net.HttpURLConnection
import javax.inject.Inject

class LogInViewModel @Inject constructor(
    private val logInUseCase: LogInUseCase,
    private val stringsManager: StringsManager
) : BaseViewModel() {

    private val _resource: MutableLiveData<Resource<String>> = MutableLiveData()

    val resource: LiveData<Resource<String>>
        get() = _resource

    fun login(user: LogInUser) {
        logInUseCase.execute(user)
            .subscribeWithResource(_resource,
                onError = {
                    val error = it as? HttpException
                    val message = if (error?.code() == HttpURLConnection.HTTP_BAD_REQUEST) {
                        stringsManager.getString(R.string.wrong_credentials_error)
                    } else {
                        it.localizedMessage
                    }
                    _resource.postValue(Resource.error(message))
                })
            .addTo(disposables)
    }
}
