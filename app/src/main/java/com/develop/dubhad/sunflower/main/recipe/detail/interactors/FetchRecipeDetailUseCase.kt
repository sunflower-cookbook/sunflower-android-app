package com.develop.dubhad.sunflower.main.recipe.detail.interactors

import com.develop.dubhad.sunflower.main.recipe.base.models.RecipeDetail
import com.develop.dubhad.sunflower.main.recipe.base.repositories.RecipeRepository
import com.develop.dubhad.sunflower.main.tag.repositories.TagRepository
import io.reactivex.Single
import io.reactivex.rxkotlin.Singles
import javax.inject.Inject

class FetchRecipeDetailUseCase @Inject constructor(
    private val recipeRepository: RecipeRepository,
    private val tagRepository: TagRepository
) {
    fun execute(recipeId: Int): Single<RecipeDetail> {
        return Singles.zip(
            recipeRepository.fetchRecipe(recipeId),
            recipeRepository.fetchRecipeIngredients(recipeId),
            recipeRepository.fetchRecipeSteps(recipeId),
            tagRepository.fetchRecipeTags(recipeId)
        ) { recipe, ingredients, steps, tags ->
            RecipeDetail(
                recipe,
                ingredients,
                steps,
                tags
            )
        }
    }
}
