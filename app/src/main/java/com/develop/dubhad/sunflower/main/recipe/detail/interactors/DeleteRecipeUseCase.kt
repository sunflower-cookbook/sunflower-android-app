package com.develop.dubhad.sunflower.main.recipe.detail.interactors

import com.develop.dubhad.sunflower.main.recipe.base.repositories.RecipeRepository
import io.reactivex.Completable
import javax.inject.Inject

class DeleteRecipeUseCase @Inject constructor(private val recipeRepository: RecipeRepository) {
    fun execute(recipeId: Int): Completable {
        return recipeRepository.deleteRecipe(recipeId)
    }
}
