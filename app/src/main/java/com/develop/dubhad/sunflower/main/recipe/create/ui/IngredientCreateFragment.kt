package com.develop.dubhad.sunflower.main.recipe.create.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.forEach
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.base.ui.BaseFragment
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Ingredient
import com.develop.dubhad.sunflower.main.recipe.create.viewmodels.RecipeCreateViewModel
import com.develop.dubhad.sunflower.utils.di.Injectable
import kotlinx.android.synthetic.main.fragment_ingredient_create.*
import kotlinx.android.synthetic.main.view_ingredient_create.view.*
import kotlinx.android.synthetic.main.view_ingredients_add.view.*
import javax.inject.Inject

class IngredientCreateFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RecipeCreateViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_ingredient_create, container, false)

        viewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(RecipeCreateViewModel::class.java)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        create_ingredient_next_step_button.setOnClickListener {
            val ingredients = mutableListOf<Ingredient>()
            ingredients_add_view.view_ingredients_container.forEach { ingr ->
                ingredients.add(Ingredient(
                    0,
                    ingr.view_ingredient_quantity.text.toString().toFloat(),
                    ingr.view_ingredient_measure.text.toString(),
                    ingr.view_ingredient_product.text.toString()
                ))
            }
            viewModel.setIngredients(ingredients)
            val direction = IngredientCreateFragmentDirections.actionIngredientCreateFragmentToRecipeStepCreateFragment()
            it.findNavController().navigate(direction)
        }
    }
}
