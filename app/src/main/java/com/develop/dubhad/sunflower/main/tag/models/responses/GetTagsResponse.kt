package com.develop.dubhad.sunflower.main.tag.models.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GetTagsResponse(@Json(name = "results") val results: List<TagResponse>)
