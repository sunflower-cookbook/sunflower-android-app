package com.develop.dubhad.sunflower.main.tag.interactors

import com.develop.dubhad.sunflower.main.tag.models.domain.Tag
import com.develop.dubhad.sunflower.main.tag.repositories.TagRepository
import io.reactivex.Single
import javax.inject.Inject

class FilterTagsUseCase @Inject constructor(private val tagRepository: TagRepository) {

    fun execute(query: String): Single<List<Tag>> = tagRepository.fetchTagsByQuery(query)
}
