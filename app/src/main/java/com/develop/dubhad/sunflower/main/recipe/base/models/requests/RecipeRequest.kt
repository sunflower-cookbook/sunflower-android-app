package com.develop.dubhad.sunflower.main.recipe.base.models.requests

import com.develop.dubhad.sunflower.main.recipe.base.models.RecipeDetail
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RecipeRequest(
    @Json(name = "title") val title: String,
    @Json(name = "description") val description: String = "",
    @Json(name = "photo") val photo: String = "",
    @Json(name = "cooking_time") val cookingTime: Int = 0,
    @Json(name = "person_count") val personCount: Int = 0,
    @Json(name = "ingredients") val ingredients: List<IngredientRequest>,
    @Json(name = "recipe_steps") val recipeSteps: List<RecipeStepRequest>
) {
    constructor(recipeDetail: RecipeDetail) : this(
        title = recipeDetail.recipe.title,
        description = recipeDetail.recipe.description,
        photo = recipeDetail.recipe.photo,
        cookingTime = recipeDetail.recipe.cookingTime,
        personCount = recipeDetail.recipe.personCount,
        ingredients = recipeDetail.ingredients.map(::IngredientRequest),
        recipeSteps = recipeDetail.steps.map(::RecipeStepRequest)
    )
}
