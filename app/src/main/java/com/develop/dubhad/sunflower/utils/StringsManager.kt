package com.develop.dubhad.sunflower.utils

import android.app.Application
import androidx.annotation.StringRes
import dagger.Reusable
import javax.inject.Inject

@Reusable
class StringsManager @Inject constructor(private val application: Application) {

    fun getString(@StringRes resId: Int): String = application.getString(resId)
}
