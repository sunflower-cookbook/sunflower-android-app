package com.develop.dubhad.sunflower.main.local.models

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.develop.dubhad.sunflower.main.recipe.base.models.RecipeDetail
import java.util.Date

data class RecipeDetailEntity(
    @Embedded var recipe: RecipeEntity = RecipeEntity(0, "", "", "", Date(), 0, 0, 0f, 0),
    @Relation(
        parentColumn = "recipeId",
        entityColumn = "recipeId"
    )
    var ingredients: List<IngredientEntity> = emptyList(),
    @Relation(
        parentColumn = "recipeId",
        entityColumn = "recipeId"
    )
    var steps: List<RecipeStepEntity> = emptyList(),
    @Relation(
        parentColumn = "recipeId",
        entityColumn = "tagId",
        associateBy = Junction(RecipeTagCrossRef::class)
    )
    var tags: List<TagEntity> = emptyList()
) {
    constructor(recipeDetail: RecipeDetail) : this(
        recipe = RecipeEntity(recipeDetail.recipe),
        ingredients = recipeDetail.ingredients.map { IngredientEntity(it, recipeDetail.recipe.id) },
        steps = recipeDetail.steps.map { RecipeStepEntity(it, recipeDetail.recipe.id) },
        tags = recipeDetail.tags.map(::TagEntity)
    )
}
