package com.develop.dubhad.sunflower.main.base.di

import com.develop.dubhad.sunflower.main.cookbook.ui.CookbookListFragment
import com.develop.dubhad.sunflower.main.recipe.create.ui.IngredientCreateFragment
import com.develop.dubhad.sunflower.main.recipe.create.ui.RecipeCreateFragment
import com.develop.dubhad.sunflower.main.recipe.create.ui.RecipeStepCreateFragment
import com.develop.dubhad.sunflower.main.recipe.detail.ui.RecipeDetailFragment
import com.develop.dubhad.sunflower.main.recipe.list.ui.RecipeListFragment
import com.develop.dubhad.sunflower.main.tag.ui.TagListFragment
import com.develop.dubhad.sunflower.settings.SettingsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeCookbookListFragment(): CookbookListFragment

    @ContributesAndroidInjector
    abstract fun contributeRecipeListFragment(): RecipeListFragment

    @ContributesAndroidInjector
    abstract fun contributeRecipeDetailFragment(): RecipeDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeTagListFragment(): TagListFragment

    @ContributesAndroidInjector
    abstract fun contributeRecipeCreateFragment(): RecipeCreateFragment

    @ContributesAndroidInjector
    abstract fun contributeIngredientCreateFragment(): IngredientCreateFragment

    @ContributesAndroidInjector
    abstract fun contributeRecipeStepCreateFragment(): RecipeStepCreateFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingsFragment(): SettingsFragment
}
