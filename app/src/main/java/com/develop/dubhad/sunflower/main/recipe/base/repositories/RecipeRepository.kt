package com.develop.dubhad.sunflower.main.recipe.base.repositories

import com.develop.dubhad.sunflower.base.services.SunflowerApi
import com.develop.dubhad.sunflower.main.local.dao.AuthorDao
import com.develop.dubhad.sunflower.main.local.dao.RecipeDao
import com.develop.dubhad.sunflower.main.local.models.AuthorEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeDetailEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeEntity
import com.develop.dubhad.sunflower.main.recipe.base.models.RecipeDetail
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Ingredient
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Recipe
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.RecipeStep
import com.develop.dubhad.sunflower.main.recipe.base.models.requests.RateRequest
import com.develop.dubhad.sunflower.main.recipe.base.models.requests.RecipeRequest
import com.develop.dubhad.sunflower.utils.TokenManager
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RecipeRepository @Inject constructor(
    private val sunflowerApi: SunflowerApi,
    private val recipeDao: RecipeDao,
    private val authorDao: AuthorDao
) {

    @Inject
    lateinit var tokenManager: TokenManager

    fun fetchRecipes(): Single<List<Recipe>> {
        return sunflowerApi.getRecipes()
            .map { it.results.map(::Recipe) }
    }

    fun fetchCookbookRecipes(cookbookId: Int): Single<List<Recipe>> {
        return sunflowerApi.getCookbookRecipes(cookbookId, tokenManager.getToken())
            .map { it.results.map(::Recipe) }
    }

    fun fetchTagRecipes(tagId: Int): Single<List<Recipe>> {
        return sunflowerApi.getTagRecipes(tagId)
            .map { it.results.map(::Recipe) }
    }

    fun fetchRecipe(recipeId: Int): Single<Recipe> {
        return sunflowerApi.getRecipe(recipeId)
            .map(::Recipe)
    }

    fun fetchRecipeIngredients(recipeId: Int): Single<List<Ingredient>> {
        return sunflowerApi.getRecipeIngredients(recipeId)
            .map { it.map(::Ingredient) }
    }

    fun fetchRecipeSteps(recipeId: Int): Single<List<RecipeStep>> {
        return sunflowerApi.getRecipeSteps(recipeId)
            .map { it.map(::RecipeStep) }
    }

    fun rateRecipe(rating: Float, recipeId: Int): Completable {
        return sunflowerApi.rateRecipe(RateRequest(rating), recipeId, tokenManager.getToken())
    }

    fun deleteRecipe(recipeId: Int): Completable {
        return sunflowerApi.deleteRecipe(recipeId, tokenManager.getToken())
    }

    fun postRecipe(recipe: RecipeDetail): Completable {
        return sunflowerApi.postRecipe(RecipeRequest(recipe), tokenManager.getToken())
    }

    fun getLocalRecipes(): Single<List<Recipe>> {
        return recipeDao.getAllRecipes()
            .map { it.map { entity -> Recipe(entity, authorDao.getAuthor(entity.authorId)) } }
            .subscribeOn(Schedulers.io())
    }

    fun getLocalRecipe(recipeId: Int): Single<RecipeDetail> {
        return recipeDao.getFullRecipe(recipeId)
            .map { RecipeDetail(it, authorDao.getAuthor(it.recipe.authorId)) }
            .subscribeOn(Schedulers.io())
    }

    fun saveRecipeLocal(recipeDetail: RecipeDetail): Completable {
        return authorDao.insertAuthor(AuthorEntity(recipeDetail.recipe.author))
            .andThen(recipeDao.insertFullRecipe(RecipeDetailEntity(recipeDetail)))
            .subscribeOn(Schedulers.io())
    }

    fun deleteLocalRecipe(recipe: Recipe): Completable {
        return recipeDao.deleteRecipe(RecipeEntity(recipe))
            .subscribeOn(Schedulers.io())
    }
}
