package com.develop.dubhad.sunflower.main.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.develop.dubhad.sunflower.main.local.models.IngredientEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeDetailEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeStepEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeTagCrossRef
import com.develop.dubhad.sunflower.main.local.models.TagEntity
import io.reactivex.Completable
import io.reactivex.Single

@Dao
abstract class RecipeDao {

    open fun insertFullRecipe(fullRecipe: RecipeDetailEntity): Completable {
        val recipeTagRefs =
            fullRecipe.tags.map { RecipeTagCrossRef(fullRecipe.recipe.recipeId, it.tagId) }
        return insertRecipe(fullRecipe.recipe)
            .andThen(insertIngredients(fullRecipe.ingredients))
            .andThen(insertRecipeSteps(fullRecipe.steps))
            .andThen(insertTags(fullRecipe.tags))
            .andThen(insertRecipeTagRefs(recipeTagRefs))
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertRecipe(recipe: RecipeEntity): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertIngredients(ingredients: List<IngredientEntity>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertRecipeSteps(recipeSteps: List<RecipeStepEntity>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertTags(tags: List<TagEntity>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertRecipeTagRefs(recipeTagRefs: List<RecipeTagCrossRef>): Completable

    @Transaction
    @Query("SELECT * FROM RecipeEntity WHERE recipeId == :recipeId")
    abstract fun getFullRecipe(recipeId: Int): Single<RecipeDetailEntity>

    @Query("SELECT * FROM RecipeEntity")
    abstract fun getAllRecipes(): Single<List<RecipeEntity>>

    @Delete
    abstract fun deleteRecipe(recipe: RecipeEntity): Completable
}
