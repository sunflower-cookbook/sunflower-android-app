package com.develop.dubhad.sunflower.main.recipe.detail.ui

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.develop.dubhad.sunflower.R

class AddRecipeToCookbookFragment : DialogFragment() {

    internal lateinit var listener: AddRecipeToCookbookListener

    interface AddRecipeToCookbookListener {
        fun onDialogPositiveClick(selected: ArrayList<Int>, unselected: ArrayList<Int>)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            listener = targetFragment as AddRecipeToCookbookListener

            val cookbookNames =
                arguments?.getCharSequenceArray(getString(R.string.add_recipe_to_cookbook_cookbook_names_key))
            val checkedNames =
                arguments?.getBooleanArray(getString(R.string.add_recipe_to_cookbook_checked_cookbooks))
            val selectedItems = ArrayList<Int>()
            val unselectedItems = ArrayList<Int>()

            val builder = AlertDialog.Builder(it)
            builder.setTitle(getString(R.string.choose_cookbooks_title))
                .setMultiChoiceItems(cookbookNames, checkedNames) { _, which, isChecked ->
                    if (isChecked) {
                        selectedItems.add(which)
                        unselectedItems.remove(Integer.valueOf(which))
                    } else {
                        selectedItems.remove(Integer.valueOf(which))
                        unselectedItems.add(which)
                    }
                }
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    listener.onDialogPositiveClick(selectedItems, unselectedItems)
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}
