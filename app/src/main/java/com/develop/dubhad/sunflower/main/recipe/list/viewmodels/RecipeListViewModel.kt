package com.develop.dubhad.sunflower.main.recipe.list.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.develop.dubhad.sunflower.base.models.Resource
import com.develop.dubhad.sunflower.base.viewmodels.BaseViewModel
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Recipe
import com.develop.dubhad.sunflower.main.recipe.list.interactors.FetchCookbookRecipesUseCase
import com.develop.dubhad.sunflower.main.recipe.list.interactors.FetchRecipesUseCase
import com.develop.dubhad.sunflower.main.recipe.list.interactors.FetchTagRecipesUseCase
import com.develop.dubhad.sunflower.main.recipe.list.interactors.GetLocalRecipesUseCase
import com.develop.dubhad.sunflower.utils.extensions.subscribeWithResource
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class RecipeListViewModel @Inject constructor(
    private val fetchRecipesUseCase: FetchRecipesUseCase,
    private val fetchTagRecipesUseCase: FetchTagRecipesUseCase,
    private val fetchCookbookRecipesUseCase: FetchCookbookRecipesUseCase,
    private val getLocalRecipesUseCase: GetLocalRecipesUseCase
) : BaseViewModel() {

    private val _cookbookId: MutableLiveData<Int> = MutableLiveData()
    private val _tagId: MutableLiveData<Int> = MutableLiveData()

    private val _resource: MutableLiveData<Resource<List<Recipe>>> = MutableLiveData()

    val resource: LiveData<Resource<List<Recipe>>>
        get() = _resource

    fun setCookbookId(cookBookId: Int) {
        _cookbookId.value = cookBookId
    }

    fun setTagId(tagId: Int) {
        _tagId.value = tagId
    }

    fun fetchRecipes() {
        fetchRecipesUseCase.execute()
            .subscribeWithResource(_resource)
            .addTo(disposables)
    }

    fun fetchCookbookRecipes() {
        _cookbookId.value?.let { cookbookId ->
            fetchCookbookRecipesUseCase.execute(cookbookId)
                .subscribeWithResource(_resource)
                .addTo(disposables)
        }
    }

    fun fetchTagRecipes() {
        _tagId.value?.let { tagId ->
            fetchTagRecipesUseCase.execute(tagId)
                .subscribeWithResource(_resource)
                .addTo(disposables)
        }
    }

    fun getLocalRecipes() {
        getLocalRecipesUseCase.execute()
            .subscribeWithResource(_resource)
            .addTo(disposables)
    }
}
