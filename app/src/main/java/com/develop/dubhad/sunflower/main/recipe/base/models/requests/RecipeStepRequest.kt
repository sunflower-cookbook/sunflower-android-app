package com.develop.dubhad.sunflower.main.recipe.base.models.requests

import com.develop.dubhad.sunflower.main.recipe.base.models.domain.RecipeStep
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RecipeStepRequest(
    @Json(name = "description") val description: String,
    @Json(name = "photo") val photo: String = "",
    @Json(name = "step_number") val stepNumber: Int
) {
    constructor(recipeStep: RecipeStep) : this(
        description = recipeStep.description,
        photo = recipeStep.photo,
        stepNumber = recipeStep.number
    )
}
