package com.develop.dubhad.sunflower.utils

import android.content.SharedPreferences
import com.develop.dubhad.sunflower.R
import dagger.Reusable
import javax.inject.Inject

@Reusable
class PrefsManager @Inject constructor(
    private val sharedPrefs: SharedPreferences,
    private val stringsManager: StringsManager
) {

    private val getString = { resId: Int ->
        stringsManager.getString(resId)
    }

    companion object {
        private const val PREF_SKIP_AUTH = "PREF_SKIP_AUTH"
        private const val PREF_GESTURE_DELAY = "PREF_GESTURE_DELAY"
        private const val PREF_SCROLL_DELTA = "PREF_SCROLL_DELTA"
        private const val PREF_REMOTE_URL = "PREF_REMOTE_URL"

        private const val DEFAULT_SCROLL_DELTA = 100
    }

    var prefSkipAuth: Boolean
        get() = sharedPrefs.getBoolean(PREF_SKIP_AUTH, false)
        set(value) {
            sharedPrefs.edit()
                .putBoolean(PREF_SKIP_AUTH, value)
                .apply()
        }

    var prefGestureDelay: Int
        get() = sharedPrefs.getInt(PREF_GESTURE_DELAY, 1)
        set(value) {
            sharedPrefs.edit()
                .putInt(PREF_GESTURE_DELAY, value)
                .apply()
        }

    var prefScrollDelta: Int
        get() = sharedPrefs.getInt(PREF_SCROLL_DELTA, DEFAULT_SCROLL_DELTA)
        set(value) {
            sharedPrefs.edit()
                .putInt(PREF_SCROLL_DELTA, value)
                .apply()
        }

    var prefGestureControl: Boolean
        get() = sharedPrefs.getBoolean(getString(R.string.pref_gesture_control), false)
        set(value) {
            sharedPrefs.edit()
                .putBoolean(getString(R.string.pref_gesture_control), value)
                .apply()
        }

    var prefVoiceControl: Boolean
        get() = sharedPrefs.getBoolean(getString(R.string.pref_voice_control), false)
        set(value) {
            sharedPrefs.edit()
                .putBoolean(getString(R.string.pref_voice_control), value)
                .apply()
        }

    var prefRemoteUrl: String
        get() = sharedPrefs.getString(PREF_REMOTE_URL, DEFAULT_REMOTE_URL) ?: DEFAULT_REMOTE_URL
        set(value) {
            sharedPrefs.edit()
                .putString(PREF_REMOTE_URL, value)
                .apply()
        }
}
