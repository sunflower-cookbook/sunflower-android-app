package com.develop.dubhad.sunflower.base.di.modules

import android.app.Application
import androidx.room.Room
import com.develop.dubhad.sunflower.main.local.AppDatabase
import com.develop.dubhad.sunflower.main.local.dao.AuthorDao
import com.develop.dubhad.sunflower.main.local.dao.RecipeDao
import com.develop.dubhad.sunflower.utils.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DatabaseModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @JvmStatic
    @Provides
    @Singleton
    fun provideAuthorDao(database: AppDatabase): AuthorDao {
        return database.authorDao()
    }

    @JvmStatic
    @Provides
    @Singleton
    fun provideRecipeDao(database: AppDatabase): RecipeDao {
        return database.recipeDao()
    }
}
