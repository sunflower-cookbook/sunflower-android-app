package com.develop.dubhad.sunflower.main.recipe.base.models.requests

import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Ingredient
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class IngredientRequest(
    @Json(name = "quantity") val quantity: Float,
    @Json(name = "measure") val weightMeasure: String,
    @Json(name = "product") val productName: String
) {
    constructor(ingredient: Ingredient) : this(
        quantity = ingredient.quantity,
        weightMeasure = ingredient.weightMeasure,
        productName = ingredient.productName
    )
}
