package com.develop.dubhad.sunflower.main.local.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.develop.dubhad.sunflower.main.author.models.domain.Author

@Entity
data class AuthorEntity(@PrimaryKey val id: Int, val username: String) {
    constructor(author: Author) : this(
        id = author.id,
        username = author.username
    )
}
