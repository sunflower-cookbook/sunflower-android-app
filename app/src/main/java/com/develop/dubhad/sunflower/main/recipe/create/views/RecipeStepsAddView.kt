package com.develop.dubhad.sunflower.main.recipe.create.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.view.forEach
import com.develop.dubhad.sunflower.R
import kotlinx.android.synthetic.main.view_recipe_step_create.view.*
import kotlinx.android.synthetic.main.view_recipe_steps_add.view.*

class RecipeStepsAddView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    init {
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.view_recipe_steps_add, this)

        view_add_step_button.setOnClickListener {
            addStepView(context, attrs)
        }

        addStepView(context, attrs)
    }

    private fun addStepView(context: Context, attrs: AttributeSet) {
        view_steps_container.addView(RecipeStepCreateView(context, attrs))
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        view_steps_container.forEach {
            it.view_step_create_number.text = ((it.parent as ViewGroup).indexOfChild(it) + 1).toString()
        }
    }
}
