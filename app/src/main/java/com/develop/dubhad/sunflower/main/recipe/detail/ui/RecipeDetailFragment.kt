package com.develop.dubhad.sunflower.main.recipe.detail.ui

import android.Manifest
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.os.ConfigurationCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.base.models.Status
import com.develop.dubhad.sunflower.base.ui.BaseFragment
import com.develop.dubhad.sunflower.main.asr.AudioEmitter
import com.develop.dubhad.sunflower.main.recipe.detail.viewmodels.RecipeDetailViewModel
import com.develop.dubhad.sunflower.utils.FormatUtil
import com.develop.dubhad.sunflower.utils.PrefsManager
import com.develop.dubhad.sunflower.utils.TokenManager
import com.develop.dubhad.sunflower.utils.di.Injectable
import com.develop.dubhad.sunflower.utils.extensions.load
import com.develop.dubhad.sunflower.utils.extensions.snack
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.snackbar.Snackbar
import com.google.api.gax.rpc.ResponseObserver
import com.google.api.gax.rpc.StreamController
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.speech.v1p1beta1.RecognitionConfig
import com.google.cloud.speech.v1p1beta1.SpeechClient
import com.google.cloud.speech.v1p1beta1.SpeechContext
import com.google.cloud.speech.v1p1beta1.SpeechSettings
import com.google.cloud.speech.v1p1beta1.StreamingRecognitionConfig
import com.google.cloud.speech.v1p1beta1.StreamingRecognizeRequest
import com.google.cloud.speech.v1p1beta1.StreamingRecognizeResponse
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.fragment_recipe_detail.*
import java.util.Calendar
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

class RecipeDetailFragment :
    BaseFragment(), AddRecipeToCookbookFragment.AddRecipeToCookbookListener, SensorEventListener,
    Injectable {

    companion object {
        private const val MILLIS_IN_SECOND = 1000
        private const val DEFAULT_ALTERNATIVE_COUNT = 10
    }

    private lateinit var sensorManager: SensorManager
    private var proximitySensor: Sensor? = null

    private var permissionToRecord = false

    private var audioEmitter: AudioEmitter? = null
    private val speechClient by lazy {
        requireActivity().applicationContext.resources.openRawResource(R.raw.credential).use {
            SpeechClient.create(
                SpeechSettings.newBuilder()
                .setCredentialsProvider { GoogleCredentials.fromStream(it) }
                .build())
        }
    }

    private var time: Double = 0.0
    private var proximity: Float = 0.0f

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var tokenManager: TokenManager

    @Inject
    lateinit var prefsManager: PrefsManager

    private lateinit var recipeDetailViewModel: RecipeDetailViewModel

    private var recipeId: Int = 0

    private var local: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_recipe_detail, container, false)

        recipeId = RecipeDetailFragmentArgs.fromBundle(requireArguments()).recipeId
        local = RecipeDetailFragmentArgs.fromBundle(requireArguments()).local

        setHasOptionsMenu(true)

        recipeDetailViewModel = ViewModelProvider(this, viewModelFactory)
            .get(RecipeDetailViewModel::class.java)
        recipeDetailViewModel.setRecipeId(recipeId)

        if (local) {
            recipeDetailViewModel.getLocalRecipe()
        } else {
            recipeDetailViewModel.fetchRecipe()
        }

        sensorManager = activity?.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY)

        Dexter.withContext(requireContext())
            .withPermission(Manifest.permission.RECORD_AUDIO)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    permissionToRecord = true
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                }
            }).check()

        return view
    }

    override fun onResume() {
        super.onResume()

        if (prefsManager.prefGestureControl) {
            proximitySensor?.also { proximity ->
                sensorManager.registerListener(this, proximity, SensorManager.SENSOR_DELAY_NORMAL)
            }
        }

        if (prefsManager.prefVoiceControl) {
            val isFirstRequest = AtomicBoolean(true)
            audioEmitter = AudioEmitter()

            if (permissionToRecord) {
                // start streaming the data to the server and collect responses
                val requestStream = speechClient.streamingRecognizeCallable()
                    .splitCall(object : ResponseObserver<StreamingRecognizeResponse> {
                        override fun onError(t: Throwable) {
                            Log.e("ASR", "an error occurred", t)
                        }

                        override fun onComplete() {
                            Log.d("ASR", "stream closed")
                        }

                        override fun onResponse(response: StreamingRecognizeResponse?) {
                            if (response != null) {
                                requireActivity().runOnUiThread {
                                    when {
                                        response.resultsCount > 0 -> {
                                            val variants = response.getResults(0).alternativesList.map { it.transcript }
                                            val result = variants.joinToString("")
                                            Log.d("ASR_result", result)

                                            val scrollDelta = prefsManager.prefScrollDelta
                                            val height = view?.height ?: 0
                                            if (result.contains(getString(R.string.command_down), true)) {
                                                if (isAppBarLayoutCollapsed()) {
                                                    recipe_info_scrollview.post {
                                                        recipe_info_scrollview.smoothScrollBy(0, height - scrollDelta)
                                                    }
                                                } else {
                                                    recipe_photo_appbar_layout.setExpanded(false)
                                                }
                                            } else if (result.contains(getString(R.string.command_up), true)) {
                                                recipe_info_scrollview.post {
                                                    recipe_info_scrollview.smoothScrollBy(0, -height + scrollDelta)
                                                }
                                            }
                                        }
                                        else -> {
                                            view?.snack("API error", Snackbar.LENGTH_SHORT)
                                        }
                                    }
                                }
                            }
                        }

                        override fun onStart(controller: StreamController?) {
                            Log.d("ASR", "stream started")
                        }
                    })

                // monitor the input stream and send requests as audio data becomes available
                audioEmitter!!.start { bytes ->
                    val builder = StreamingRecognizeRequest.newBuilder()
                        .setAudioContent(bytes)

                    val commandList = listOf(getString(R.string.command_up), getString(R.string.command_down))
                    val speechContext = SpeechContext.newBuilder().addAllPhrases(commandList).build()

                    // if first time, include the config
                    if (isFirstRequest.getAndSet(false)) {
                        val currentLocale = ConfigurationCompat.getLocales(resources.configuration)[0]
                        builder.streamingConfig = StreamingRecognitionConfig.newBuilder()
                            .setConfig(
                                RecognitionConfig.newBuilder()
                                    .setMaxAlternatives(DEFAULT_ALTERNATIVE_COUNT)
                                    .setModel("command_and_search")
                                    .setLanguageCode("${currentLocale.language}-${currentLocale.country}")
                                    .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)
                                    .setSampleRateHertz(16000)
                                    .addSpeechContexts(speechContext)
                                    .build())
                            .setInterimResults(false)
                            .setSingleUtterance(false)
                            .build()
                    }

                    // send the next request
                    requestStream.send(builder.build())
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()

        audioEmitter?.stop()
        audioEmitter = null

        sensorManager.unregisterListener(this)
    }

    override fun onDetach() {
        super.onDetach()

        speechClient.shutdownNow()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {
        val newProximity = event?.values?.firstOrNull() ?: 0.0f

        // Near
        if (newProximity < proximity) {
            time = Calendar.getInstance().timeInMillis.toDouble() / MILLIS_IN_SECOND
        }
        // Far
        else {
            val newTime = Calendar.getInstance().timeInMillis.toDouble() / MILLIS_IN_SECOND
            val diffTime = newTime - time
            val delay = prefsManager.prefGestureDelay
            val scrollDelta = prefsManager.prefScrollDelta
            val height = view?.height ?: 0
            if (diffTime < delay) {
                if (isAppBarLayoutCollapsed()) {
                    recipe_info_scrollview.post {
                        recipe_info_scrollview.smoothScrollBy(0, height - scrollDelta)
                    }
                } else {
                    recipe_photo_appbar_layout.setExpanded(false)
                }
            } else if (diffTime >= delay) {
                recipe_info_scrollview.post {
                    recipe_info_scrollview.smoothScrollBy(0, -height + scrollDelta)
                }
            }
        }
        proximity = newProximity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recipe_photo_appbar_layout.visibility = View.GONE
        recipe_info_scrollview.visibility = View.GONE

        recipeDetailViewModel.recipeResource.observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    hideLoadingBar()
                    recipe_photo_appbar_layout.visibility = View.VISIBLE
                    recipe_info_scrollview.visibility = View.VISIBLE

                    val recipeDetail = resource.data
                    val recipe = recipeDetail.recipe
                    recipe_photo_appbar_layout.visibility =
                        if (recipe.photo.isBlank()) View.GONE else View.VISIBLE

                    recipe_photo.load(recipe.photo)

                    recipe_title.text = recipe.title
                    material_rating_bar_indicator.rating = recipe.rating
                    recipe_rating.text = String.format("%.1f", recipe.rating)

                    recipe_info_card.visibility =
                        if (recipe.cookingTime == 0 || recipe.personCount == 0) {
                            View.GONE
                        } else {
                            View.VISIBLE
                        }
                    cooking_time_group.visibility =
                        if (recipe.cookingTime == 0) View.GONE else View.VISIBLE

                    recipe_cooking_time.text =
                        FormatUtil.getReadableTime(requireContext(), recipe.cookingTime)

                    person_count_group.visibility =
                        if (recipe.personCount == 0) View.GONE else View.VISIBLE

                    recipe_person_count.text = recipe.personCount.toString()

                    recipe_description_card.visibility =
                        if (recipe.description.isBlank()) View.GONE else View.VISIBLE
                    recipe_description.text = recipe.description

                    recipe_publish_date.text = FormatUtil.getFormattedDate(recipe.publishDate.time)

                    recipe_author.text = recipe.author.username

                    recipe_ingredient_list.isNestedScrollingEnabled = false
                    val ingredientAdapter = IngredientAdapter()
                    recipe_ingredient_list.adapter = ingredientAdapter
                    ingredientAdapter.submitList(recipeDetail.ingredients)

                    recipe_step_list.isNestedScrollingEnabled = false
                    val recipeStepsAdapter = RecipeStepAdapter(requireActivity())
                    recipe_step_list.adapter = recipeStepsAdapter
                    recipeStepsAdapter.submitList(recipeDetail.steps)

                    val chipGroup: ChipGroup = view.findViewById(R.id.recipe_tag_list)

                    chipGroup.removeAllViews()
                    for (tag in recipeDetail.tags) {
                        val chip = Chip(chipGroup.context)
                        if (!local) {
                            chip.setOnClickListener {
                                val direction =
                                    RecipeDetailFragmentDirections.actionRecipeDetailFragmentToRecipeListFragment(
                                        tagId = tag.id,
                                        title = tag.title
                                    )
                                it.findNavController().navigate(direction)
                            }
                        }
                        chip.text = tag.title
                        chipGroup.addView(chip)
                    }

                    rating_bar_group.visibility = if (!tokenManager.isTokenExists() || local) {
                        View.GONE
                    } else {
                        View.VISIBLE
                    }
                }
                Status.ERROR -> {
                    hideLoadingBar()
                    showError(resource.errorMessage)
                }
                Status.LOADING -> {
                    recipe_photo_appbar_layout.visibility = View.GONE
                    recipe_info_scrollview.visibility = View.GONE
                    showLoadingBar()
                }
            }
        })

        recipeDetailViewModel.rateResource.observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    recipeDetailViewModel.fetchRecipe()
                    rating_bar.isEnabled = true
                    view.snack("Rated", Snackbar.LENGTH_SHORT)
                }
                Status.ERROR -> {
                    rating_bar.isEnabled = true
                    showError(resource.errorMessage)
                }
                Status.LOADING -> {
                    rating_bar.isEnabled = false
                }
            }
        })

        recipeDetailViewModel.deleteResource.observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    findNavController().navigateUp()
                }
                Status.ERROR -> {
                    showError(resource.errorMessage)
                }
                Status.LOADING -> {
                }
            }
        })

        recipeDetailViewModel.saveResource.observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    view.snack("Saved!", Snackbar.LENGTH_SHORT)
                }
                Status.ERROR -> {
                    showError(resource.errorMessage)
                }
                Status.LOADING -> {
                }
            }
        })

        recipeDetailViewModel.localDeleteResource.observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    findNavController().navigateUp()
                }
                Status.ERROR -> {
                    showError(resource.errorMessage)
                }
                Status.LOADING -> {
                }
            }
        })

        rating_bar.setOnRatingChangeListener { _, rating ->
            recipeDetailViewModel.rateRecipe(rating)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (local) {
            inflater.inflate(R.menu.fragment_recipe_detail_local, menu)
        } else {
            inflater.inflate(R.menu.fragment_recipe_detail, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_add_recipe_to_cookbook -> {
                // showAddRecipeToCookbookDialog()
                true
            }
            R.id.action_delete_recipe -> {
                recipeDetailViewModel.deleteRecipe()
                true
            }
            R.id.action_save_recipe_local -> {
                recipeDetailViewModel.saveRecipeLocal()
                true
            }
            R.id.action_delete_recipe_local -> {
                recipeDetailViewModel.deleteLocalRecipe()
                true
            }
            else -> false
        }
    }

    override fun onDialogPositiveClick(selected: ArrayList<Int>, unselected: ArrayList<Int>) {
    }

    private fun isAppBarLayoutCollapsed(): Boolean {
        return recipe_photo_appbar_layout.height - recipe_photo_appbar_layout.bottom != 0
    }
}
