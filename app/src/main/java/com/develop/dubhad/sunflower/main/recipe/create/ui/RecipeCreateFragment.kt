package com.develop.dubhad.sunflower.main.recipe.create.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.base.ui.BaseFragment
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Recipe
import com.develop.dubhad.sunflower.main.recipe.create.viewmodels.RecipeCreateViewModel
import com.develop.dubhad.sunflower.utils.di.Injectable
import kotlinx.android.synthetic.main.fragment_recipe_create.*
import javax.inject.Inject

class RecipeCreateFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RecipeCreateViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_recipe_create, container, false)

        viewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(RecipeCreateViewModel::class.java)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        create_recipe_next_step_button.setOnClickListener {
            val recipe = Recipe(
                title_field.text.toString(),
                description_field.text.toString(),
                "",
                cooking_time_field.text.toString().toIntOrNull() ?: 0,
                person_count_field.text.toString().toIntOrNull() ?: 0
            )
            viewModel.setRecipe(recipe)
            val direction = RecipeCreateFragmentDirections.actionRecipeCreateFragmentToIngredientCreateFragment()
            it.findNavController().navigate(direction)
        }
    }
}
