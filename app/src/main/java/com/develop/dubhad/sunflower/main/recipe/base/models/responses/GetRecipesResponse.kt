package com.develop.dubhad.sunflower.main.recipe.base.models.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GetRecipesResponse(@Json(name = "results") val results: List<RecipeResponse>)
