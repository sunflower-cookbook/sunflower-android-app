package com.develop.dubhad.sunflower.main.recipe.create.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.forEach
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.base.models.Status
import com.develop.dubhad.sunflower.base.ui.BaseFragment
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.RecipeStep
import com.develop.dubhad.sunflower.main.recipe.create.viewmodels.RecipeCreateViewModel
import com.develop.dubhad.sunflower.utils.di.Injectable
import com.develop.dubhad.sunflower.utils.extensions.snack
import kotlinx.android.synthetic.main.fragment_recipe_step_create.*
import kotlinx.android.synthetic.main.view_recipe_step_create.view.*
import kotlinx.android.synthetic.main.view_recipe_steps_add.view.*
import javax.inject.Inject

class RecipeStepCreateFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RecipeCreateViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_recipe_step_create, container, false)

        viewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(RecipeCreateViewModel::class.java)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        create_step_post_button.setOnClickListener {
            val steps = mutableListOf<RecipeStep>()
            recipe_steps_add_view.view_steps_container.forEach { step ->
                steps.add(
                    RecipeStep(
                        0,
                        step.view_step_create_desc.text.toString(),
                        "",
                        step.view_step_create_number.text.toString().toInt()
                    )
                )
            }
            viewModel.setRecipeSteps(steps)

            viewModel.postRecipe()
        }

        viewModel.postRecipeResource.observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    hideLoadingBar()
                    view.snack("Recipe uploaded")
                    findNavController().navigate(R.id.recipe_list_fragment)
                }
                Status.ERROR -> {
                    hideLoadingBar()
                    showError(resource.errorMessage)
                }
                Status.LOADING -> showLoadingBar()
            }
        })
    }
}
