package com.develop.dubhad.sunflower.main.recipe.list.interactors

import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Recipe
import com.develop.dubhad.sunflower.main.recipe.base.repositories.RecipeRepository
import io.reactivex.Single
import javax.inject.Inject

class FetchRecipesUseCase @Inject constructor(private val recipeRepository: RecipeRepository) {

    fun execute(): Single<List<Recipe>> = recipeRepository.fetchRecipes()
}
