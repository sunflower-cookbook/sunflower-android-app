package com.develop.dubhad.sunflower.main.local.models

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.RecipeStep

@Entity(
    foreignKeys = [ForeignKey(
        entity = RecipeEntity::class,
        parentColumns = ["recipeId"],
        childColumns = ["recipeId"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class RecipeStepEntity(
    @PrimaryKey val id: Int,
    val description: String,
    val photo: String,
    val number: Int,
    val recipeId: Int
) {
    constructor(recipeStep: RecipeStep, recipeId: Int) : this(
        id = recipeStep.id,
        description = recipeStep.description,
        photo = recipeStep.photo,
        number = recipeStep.number,
        recipeId = recipeId
    )
}
