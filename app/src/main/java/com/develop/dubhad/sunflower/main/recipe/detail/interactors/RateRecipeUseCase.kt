package com.develop.dubhad.sunflower.main.recipe.detail.interactors

import com.develop.dubhad.sunflower.main.recipe.base.repositories.RecipeRepository
import io.reactivex.Completable
import javax.inject.Inject

class RateRecipeUseCase @Inject constructor(private val recipeRepository: RecipeRepository) {

    fun execute(rating: Float, recipeId: Int): Completable {
        return recipeRepository.rateRecipe(rating, recipeId)
    }
}
