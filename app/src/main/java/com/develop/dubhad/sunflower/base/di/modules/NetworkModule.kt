package com.develop.dubhad.sunflower.base.di.modules

import com.develop.dubhad.sunflower.BuildConfig
import com.develop.dubhad.sunflower.authentication.models.errors.RegisterErrorResponse
import com.develop.dubhad.sunflower.base.services.SunflowerApi
import com.develop.dubhad.sunflower.utils.PrefsManager
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
object NetworkModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        }
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient().newBuilder()
            .addInterceptor(interceptor)
            .build()
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideRetrofit(client: OkHttpClient, prefsManager: PrefsManager): Retrofit {
        return Retrofit.Builder()
            .baseUrl(prefsManager.prefRemoteUrl)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideSunflowerApi(retrofit: Retrofit): SunflowerApi {
        return retrofit.create(SunflowerApi::class.java)
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideRegisterErrorConverter(retrofit: Retrofit): Converter<ResponseBody, RegisterErrorResponse> {
        return retrofit.responseBodyConverter(RegisterErrorResponse::class.java, arrayOf())
    }
}
