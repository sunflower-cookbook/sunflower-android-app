package com.develop.dubhad.sunflower.main.recipe.create.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.develop.dubhad.sunflower.R
import kotlinx.android.synthetic.main.view_recipe_step_create.view.*

class RecipeStepCreateView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    init {
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.view_recipe_step_create, this)

        view_step_delete_button.setOnClickListener {
            (parent as ViewGroup).removeView(this)
        }
    }
}
