package com.develop.dubhad.sunflower.authentication.interactors

import com.develop.dubhad.sunflower.authentication.models.domain.LogInUser
import com.develop.dubhad.sunflower.authentication.repositories.AuthenticationRepository
import io.reactivex.Single
import javax.inject.Inject

class LogInUseCase @Inject constructor(
    private val authenticationRepository: AuthenticationRepository
) {
    fun execute(user: LogInUser): Single<String> = authenticationRepository.login(user)
}
