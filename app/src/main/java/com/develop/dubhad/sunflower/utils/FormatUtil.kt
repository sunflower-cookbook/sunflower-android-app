package com.develop.dubhad.sunflower.utils

import android.content.Context
import androidx.annotation.StringRes
import com.develop.dubhad.sunflower.R
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

object FormatUtil {

    private const val MINUTES_IN_HOUR = 60

    fun getReadableTime(context: Context, minutes: Int): String {
        return when {
            minutes < 0 -> throw IllegalArgumentException("Value must be positive")
            minutes < MINUTES_IN_HOUR -> "$minutes ${context.getString(TimeUnit.MINUTES.resId)}"
            else -> {
                var result = "${minutes / MINUTES_IN_HOUR} ${context.getString(TimeUnit.HOURS.resId)}"
                val minutesRemainder = minutes % MINUTES_IN_HOUR
                if (minutesRemainder > 0) {
                    result += " $minutesRemainder ${context.getString(TimeUnit.MINUTES.resId)}"
                }
                result
            }
        }
    }

    fun getFormattedDate(timeInMillis: Long): String {
        val sdf = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
        return sdf.format(Date(timeInMillis))
    }

    enum class TimeUnit(@StringRes val resId: Int) {
        MINUTES(R.string.minutes),
        HOURS(R.string.hours)
    }
}
