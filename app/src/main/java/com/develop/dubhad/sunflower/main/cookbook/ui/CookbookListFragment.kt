package com.develop.dubhad.sunflower.main.cookbook.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.base.models.Status
import com.develop.dubhad.sunflower.base.ui.BaseFragment
import com.develop.dubhad.sunflower.main.cookbook.viewmodels.CookbookListViewModel
import com.develop.dubhad.sunflower.utils.di.Injectable
import kotlinx.android.synthetic.main.fragment_cookbook_list.*
import javax.inject.Inject

class CookbookListFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: CookbookListViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_cookbook_list, container, false)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(CookbookListViewModel::class.java)

        viewModel.fetchCookbooks()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = CookbookAdapter()
        cookbook_list.adapter = adapter
        subscribeUi(adapter)
    }

    private fun subscribeUi(adapter: CookbookAdapter) {
        viewModel.resource.observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    hideLoadingBar()
                    adapter.submitList(resource.data)
                }
                Status.ERROR -> {
                    hideLoadingBar()
                    showError(resource.errorMessage)
                }
                Status.LOADING -> showLoadingBar()
            }
        })
    }
}
