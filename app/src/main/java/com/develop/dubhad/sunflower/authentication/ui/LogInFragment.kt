package com.develop.dubhad.sunflower.authentication.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.authentication.models.domain.LogInUser
import com.develop.dubhad.sunflower.authentication.viewmodels.LogInViewModel
import com.develop.dubhad.sunflower.base.models.Status
import com.develop.dubhad.sunflower.base.ui.BaseFragment
import com.develop.dubhad.sunflower.main.base.ui.MainActivity
import com.develop.dubhad.sunflower.utils.PrefsManager
import com.develop.dubhad.sunflower.utils.TokenManager
import com.develop.dubhad.sunflower.utils.di.Injectable
import com.develop.dubhad.sunflower.utils.extensions.hideKeyboard
import kotlinx.android.synthetic.main.fragment_log_in.*
import javax.inject.Inject

class LogInFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var tokenManager: TokenManager

    @Inject
    lateinit var prefsManager: PrefsManager

    private lateinit var logInViewModel: LogInViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_log_in, container, false)

        logInViewModel = ViewModelProvider(this, viewModelFactory)
            .get(LogInViewModel::class.java)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registration_link_button.setOnClickListener {
            val direction = LogInFragmentDirections.actionLogInFragmentToRegistrationFragment()
            it.findNavController().navigate(direction)
        }

        logInViewModel.resource.observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    hideLoadingBar()
                    val token = resource.data
                    onLoginSuccess(token)
                }
                Status.ERROR -> {
                    hideLoadingBar()
                    showError(resource.errorMessage)
                }
                Status.LOADING -> showLoadingBar()
            }
        })

        login_field.addTextChangedListener {
            if (!TextUtils.isEmpty(it)) {
                login_layout.error = null
            }
        }
        password_field.addTextChangedListener {
            if (!TextUtils.isEmpty(it)) {
                password_layout.error = null
            }
        }

        sign_in_button.setOnClickListener { onSignInButtonClick() }

        skip_button.setOnClickListener {
            prefsManager.prefSkipAuth = true
            openMainActivity()
        }
    }

    private fun onSignInButtonClick() {
        requireActivity().hideKeyboard()

        if (!isEmptyFields()) {
            logInViewModel.login(
                LogInUser(
                    login_field.text.toString(),
                    password_field.text.toString()
                )
            )
        }
    }

    private fun onLoginSuccess(token: String) {
        tokenManager.saveToken(token)
        openMainActivity()
    }

    private fun openMainActivity() {
        val intent = Intent(requireActivity(), MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun isEmptyFields(): Boolean {
        var isEmpty = false
        if (TextUtils.isEmpty(login_field.text)) {
            login_layout.error = getString(R.string.empty_field_error)
            isEmpty = true
        }
        if (TextUtils.isEmpty(password_field.text)) {
            password_layout.error = getString(R.string.empty_field_error)
            isEmpty = true
        }
        return isEmpty
    }
}
