package com.develop.dubhad.sunflower.main.author.models.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AuthorResponse(
    @Json(name = "user_id") val userId: Int?,
    @Json(name = "username") val username: String?,
    @Json(name = "email") val email: String?
)
