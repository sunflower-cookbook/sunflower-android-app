package com.develop.dubhad.sunflower.base.services

import com.develop.dubhad.sunflower.authentication.models.requests.LoginRequest
import com.develop.dubhad.sunflower.authentication.models.requests.RegisterRequest
import com.develop.dubhad.sunflower.authentication.models.responses.TokenInfoResponse
import com.develop.dubhad.sunflower.main.cookbook.models.responses.GetCookbooksResponse
import com.develop.dubhad.sunflower.main.recipe.base.models.requests.RateRequest
import com.develop.dubhad.sunflower.main.recipe.base.models.requests.RecipeRequest
import com.develop.dubhad.sunflower.main.recipe.base.models.responses.GetRecipesResponse
import com.develop.dubhad.sunflower.main.recipe.base.models.responses.IngredientResponse
import com.develop.dubhad.sunflower.main.recipe.base.models.responses.RecipeResponse
import com.develop.dubhad.sunflower.main.recipe.base.models.responses.RecipeStepResponse
import com.develop.dubhad.sunflower.main.tag.models.responses.GetTagsResponse
import com.develop.dubhad.sunflower.main.tag.models.responses.TagResponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

interface SunflowerApi {

    @POST("login/")
    fun login(@Body request: LoginRequest): Single<TokenInfoResponse>

    @POST("registration/")
    fun register(@Body request: RegisterRequest): Single<TokenInfoResponse>

    @GET("cookbooks/")
    fun getUserCookbooks(@Header("Authorization") token: String): Single<GetCookbooksResponse>

    @GET("recipes/")
    fun getRecipes(): Single<GetRecipesResponse>

    @GET("recipes/{recipe_id}")
    fun getRecipe(@Path("recipe_id") recipeId: Int): Single<RecipeResponse>

    @GET("cookbooks/{cookbook_id}/recipes")
    fun getCookbookRecipes(
        @Path("cookbook_id") cookbookId: Int,
        @Header("Authorization") token: String
    ): Single<GetRecipesResponse>

    @GET("recipes/{recipe_id}/ingredients")
    fun getRecipeIngredients(@Path("recipe_id") recipeId: Int): Single<List<IngredientResponse>>

    @GET("recipes/{recipe_id}/steps")
    fun getRecipeSteps(@Path("recipe_id") recipeId: Int): Single<List<RecipeStepResponse>>

    @GET("tags")
    fun getTags(): Single<GetTagsResponse>

    @GET("recipes/")
    fun getTagRecipes(@Query("tag_id") tagId: Int): Single<GetRecipesResponse>

    @GET("recipes/{recipe_id}/tags")
    fun getRecipeTags(@Path("recipe_id") recipeId: Int): Single<List<TagResponse>>

    @PUT("recipes/{recipe_id}/rate")
    fun rateRecipe(
        @Body request: RateRequest,
        @Path("recipe_id") recipeId: Int,
        @Header("Authorization") token: String
    ): Completable

    @DELETE("recipes/{recipe_id}")
    fun deleteRecipe(
        @Path("recipe_id") recipeId: Int,
        @Header("Authorization") token: String
    ): Completable

    @POST("recipes/")
    fun postRecipe(
        @Body request: RecipeRequest,
        @Header("Authorization") token: String
    ): Completable
}
