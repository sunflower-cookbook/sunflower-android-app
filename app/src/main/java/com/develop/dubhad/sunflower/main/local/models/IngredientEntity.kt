package com.develop.dubhad.sunflower.main.local.models

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Ingredient

@Entity(
    foreignKeys = [ForeignKey(
        entity = RecipeEntity::class,
        parentColumns = ["recipeId"],
        childColumns = ["recipeId"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class IngredientEntity(
    @PrimaryKey val id: Int,
    val quantity: Float,
    val weightMeasure: String,
    val productName: String,
    val recipeId: Int
) {
    constructor(ingredient: Ingredient, recipeId: Int) : this(
        id = ingredient.id,
        quantity = ingredient.quantity,
        weightMeasure = ingredient.weightMeasure,
        productName = ingredient.productName,
        recipeId = recipeId
    )
}
