package com.develop.dubhad.sunflower.main.tag.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.develop.dubhad.sunflower.base.models.Resource
import com.develop.dubhad.sunflower.base.viewmodels.BaseViewModel
import com.develop.dubhad.sunflower.main.tag.interactors.FetchTagsUseCase
import com.develop.dubhad.sunflower.main.tag.interactors.FilterTagsUseCase
import com.develop.dubhad.sunflower.main.tag.models.domain.Tag
import com.develop.dubhad.sunflower.utils.extensions.subscribeWithResource
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class TagListViewModel @Inject constructor(
    private val fetchTagsUseCase: FetchTagsUseCase,
    private val filterTagsUseCase: FilterTagsUseCase
) : BaseViewModel() {

    private val _resource: MutableLiveData<Resource<List<Tag>>> = MutableLiveData()

    val resource: LiveData<Resource<List<Tag>>>
        get() = _resource

    fun fetchTags() {
        fetchTagsUseCase.execute()
            .subscribeWithResource(_resource)
            .addTo(disposables)
    }

    fun search(query: String) {
        filterTagsUseCase.execute(query)
            .subscribeWithResource(_resource)
            .addTo(disposables)
    }
}
