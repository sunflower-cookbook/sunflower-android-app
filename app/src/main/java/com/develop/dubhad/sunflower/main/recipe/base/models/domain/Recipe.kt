package com.develop.dubhad.sunflower.main.recipe.base.models.domain

import com.develop.dubhad.sunflower.base.exceptions.ParsingServerException
import com.develop.dubhad.sunflower.main.author.models.domain.Author
import com.develop.dubhad.sunflower.main.local.models.AuthorEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeEntity
import com.develop.dubhad.sunflower.main.recipe.base.models.responses.RecipeResponse
import com.develop.dubhad.sunflower.utils.ParsingUtil
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

data class Recipe(
    val id: Int,
    val title: String,
    val description: String,
    val photo: String,
    val publishDate: Date,
    val cookingTime: Int,
    val personCount: Int,
    val rating: Float,
    val author: Author
) {
    constructor(recipeResponse: RecipeResponse) : this(
        id = recipeResponse.recipeId ?: throw ParsingServerException(),
        title = recipeResponse.title ?: throw ParsingServerException(),
        description = recipeResponse.description ?: throw ParsingServerException(),
        photo = recipeResponse.photo ?: throw ParsingServerException(),
        publishDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            .parse(recipeResponse.publishDate),
        cookingTime = recipeResponse.cookingTime ?: throw ParsingServerException(),
        personCount = recipeResponse.personCount ?: throw ParsingServerException(),
        rating = recipeResponse.rating ?: throw ParsingServerException(),
        author = Author(
            recipeResponse.author ?: throw ParsingServerException()
        )

    ) {
        if (ParsingUtil.isAnyNumberZero(id) || ParsingUtil.isAnyStringBlank(title)) {
            throw ParsingServerException()
        }
    }
    constructor(recipeEntity: RecipeEntity, authorEntity: AuthorEntity) : this(
        id = recipeEntity.recipeId,
        title = recipeEntity.title,
        description = recipeEntity.description,
        photo = recipeEntity.photo,
        publishDate = recipeEntity.publishDate,
        cookingTime = recipeEntity.cookingTime,
        personCount = recipeEntity.personCount,
        rating = recipeEntity.rating,
        author = Author(authorEntity.id, authorEntity.username)
    )
    constructor(title: String, description: String, photo: String, cookingTime: Int, personCount: Int) : this(
        id = 0,
        title = title,
        description = description,
        photo = photo,
        publishDate = Date(),
        cookingTime = cookingTime,
        personCount = personCount,
        rating = 0f,
        author = Author(0, "")
    )
}
