package com.develop.dubhad.sunflower.main.recipe.create.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.develop.dubhad.sunflower.base.models.Resource
import com.develop.dubhad.sunflower.base.viewmodels.BaseViewModel
import com.develop.dubhad.sunflower.main.recipe.base.models.RecipeDetail
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Ingredient
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Recipe
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.RecipeStep
import com.develop.dubhad.sunflower.main.recipe.create.interactors.PostRecipeUseCase
import com.develop.dubhad.sunflower.utils.extensions.subscribeWithResource
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class RecipeCreateViewModel @Inject constructor(
    private val postRecipeUseCase: PostRecipeUseCase
) : BaseViewModel() {

    private val _recipe: MutableLiveData<Recipe> = MutableLiveData()

    private val _ingredients: MutableLiveData<List<Ingredient>> = MutableLiveData()

    private val _recipeSteps: MutableLiveData<List<RecipeStep>> = MutableLiveData()

    private val _postRecipeResource: MutableLiveData<Resource<Nothing>> = MutableLiveData()

    val postRecipeResource: LiveData<Resource<Nothing>>
        get() = _postRecipeResource

    fun setRecipe(recipe: Recipe) {
        _recipe.value = recipe
    }

    fun setIngredients(ingredients: List<Ingredient>) {
        _ingredients.value = ingredients
    }

    fun setRecipeSteps(steps: List<RecipeStep>) {
        _recipeSteps.value = steps
    }

    fun postRecipe() {
        val recipe = RecipeDetail(_recipe.value!!, _ingredients.value!!, _recipeSteps.value!!, emptyList())
        postRecipeUseCase.execute(recipe)
            .subscribeWithResource(_postRecipeResource)
            .addTo(disposables)
    }
}
