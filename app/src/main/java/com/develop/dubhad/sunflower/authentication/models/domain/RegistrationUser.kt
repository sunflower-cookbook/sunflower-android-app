package com.develop.dubhad.sunflower.authentication.models.domain

data class RegistrationUser(
    val username: String,
    val email: String,
    val password: String,
    val repeatedPassword: String
)
