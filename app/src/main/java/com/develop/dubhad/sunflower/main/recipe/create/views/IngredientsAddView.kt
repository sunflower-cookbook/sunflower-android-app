package com.develop.dubhad.sunflower.main.recipe.create.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.develop.dubhad.sunflower.R
import kotlinx.android.synthetic.main.view_ingredients_add.view.*

class IngredientsAddView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    init {
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.view_ingredients_add, this)

        view_add_ingredient_button.setOnClickListener {
            addIngredientView(context, attrs)
        }

        addIngredientView(context, attrs)
    }

    private fun addIngredientView(context: Context, attrs: AttributeSet) {
        view_ingredients_container.addView(IngredientCreateView(context, attrs))
    }
}
