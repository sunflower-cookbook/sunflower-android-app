package com.develop.dubhad.sunflower.main.recipe.detail.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.develop.dubhad.sunflower.base.models.Resource
import com.develop.dubhad.sunflower.base.viewmodels.BaseViewModel
import com.develop.dubhad.sunflower.main.recipe.base.models.RecipeDetail
import com.develop.dubhad.sunflower.main.recipe.detail.interactors.DeleteLocalRecipeUseCase
import com.develop.dubhad.sunflower.main.recipe.detail.interactors.DeleteRecipeUseCase
import com.develop.dubhad.sunflower.main.recipe.detail.interactors.FetchRecipeDetailUseCase
import com.develop.dubhad.sunflower.main.recipe.detail.interactors.GetLocalRecipeUseCase
import com.develop.dubhad.sunflower.main.recipe.detail.interactors.RateRecipeUseCase
import com.develop.dubhad.sunflower.main.recipe.detail.interactors.SaveRecipeLocalUseCase
import com.develop.dubhad.sunflower.utils.extensions.subscribeWithResource
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class RecipeDetailViewModel @Inject constructor(
    private val fetchRecipeDetailUseCase: FetchRecipeDetailUseCase,
    private val rateRecipeUseCase: RateRecipeUseCase,
    private val deleteRecipeUseCase: DeleteRecipeUseCase,
    private val getLocalRecipeUseCase: GetLocalRecipeUseCase,
    private val saveRecipeLocalUseCase: SaveRecipeLocalUseCase,
    private val deleteLocalRecipeUseCase: DeleteLocalRecipeUseCase
) : BaseViewModel() {

    private val _recipeId: MutableLiveData<Int> = MutableLiveData()

    private val _recipeResource: MutableLiveData<Resource<RecipeDetail>> = MutableLiveData()

    private val _rateResource: MutableLiveData<Resource<Nothing>> = MutableLiveData()

    private val _deleteResource: MutableLiveData<Resource<Nothing>> = MutableLiveData()

    private val _saveResource: MutableLiveData<Resource<Nothing>> = MutableLiveData()

    private val _localDeleteResource: MutableLiveData<Resource<Nothing>> = MutableLiveData()

    val recipeResource: LiveData<Resource<RecipeDetail>>
        get() = _recipeResource

    val rateResource: LiveData<Resource<Nothing>>
        get() = _rateResource

    val deleteResource: LiveData<Resource<Nothing>>
        get() = _deleteResource

    val saveResource: LiveData<Resource<Nothing>>
        get() = _saveResource

    val localDeleteResource: LiveData<Resource<Nothing>>
        get() = _localDeleteResource

    fun setRecipeId(recipeId: Int) {
        _recipeId.value = recipeId
    }

    fun fetchRecipe() {
        _recipeId.value?.let { recipeId ->
            fetchRecipeDetailUseCase.execute(recipeId)
                .subscribeWithResource(_recipeResource)
                .addTo(disposables)
        }
    }

    fun rateRecipe(rating: Float) {
        _recipeId.value?.let { recipeId ->
            rateRecipeUseCase.execute(rating, recipeId)
                .subscribeWithResource(_rateResource)
                .addTo(disposables)
        }
    }

    fun deleteRecipe() {
        _recipeId.value?.let { recipeId ->
            deleteRecipeUseCase.execute(recipeId)
                .subscribeWithResource(_deleteResource)
                .addTo(disposables)
        }
    }

    fun getLocalRecipe() {
        _recipeId.value?.let { recipeId ->
            getLocalRecipeUseCase.execute(recipeId)
                .subscribeWithResource(_recipeResource)
                .addTo(disposables)
        }
    }

    fun saveRecipeLocal() {
        if (recipeResource.value != null) {
            saveRecipeLocalUseCase.execute(recipeResource.value!!.data)
                .subscribeWithResource(_saveResource)
                .addTo(disposables)
        }
    }

    fun deleteLocalRecipe() {
        if (recipeResource.value != null) {
            deleteLocalRecipeUseCase.execute(recipeResource.value!!.data.recipe)
                .subscribeWithResource(_localDeleteResource)
                .addTo(disposables)
        }
    }
}
