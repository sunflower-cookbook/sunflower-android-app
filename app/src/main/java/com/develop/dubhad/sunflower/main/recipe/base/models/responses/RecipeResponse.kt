package com.develop.dubhad.sunflower.main.recipe.base.models.responses

import com.develop.dubhad.sunflower.main.author.models.responses.AuthorResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RecipeResponse(
    @Json(name = "recipe_id") val recipeId: Int?,
    @Json(name = "title") val title: String?,
    @Json(name = "description") val description: String?,
    @Json(name = "photo") val photo: String?,
    @Json(name = "publish_date") val publishDate: String?,
    @Json(name = "cooking_time") val cookingTime: Int?,
    @Json(name = "person_count") val personCount: Int?,
    @Json(name = "rating") val rating: Float?,
    @Json(name = "author") val author: AuthorResponse?
)
