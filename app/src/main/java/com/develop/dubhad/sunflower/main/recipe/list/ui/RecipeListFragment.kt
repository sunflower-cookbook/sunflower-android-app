package com.develop.dubhad.sunflower.main.recipe.list.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.base.models.Status
import com.develop.dubhad.sunflower.base.ui.BaseFragment
import com.develop.dubhad.sunflower.main.recipe.list.viewmodels.RecipeListViewModel
import com.develop.dubhad.sunflower.utils.di.Injectable
import com.develop.dubhad.sunflower.utils.extensions.snack
import kotlinx.android.synthetic.main.fragment_recipe_list.*
import javax.inject.Inject

class RecipeListFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RecipeListViewModel

    private var local: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_recipe_list, container, false)

        val args = arguments
        var cookbookId = 0
        var tagId = 0
        args?.let {
            cookbookId = RecipeListFragmentArgs.fromBundle(args).cookbookId
            tagId = RecipeListFragmentArgs.fromBundle(args).tagId
            local = RecipeListFragmentArgs.fromBundle(args).local
        }

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(RecipeListViewModel::class.java)

        viewModel.setCookbookId(cookbookId)
        viewModel.setTagId(tagId)

        when {
            cookbookId != 0 -> viewModel.fetchCookbookRecipes()
            tagId != 0 -> viewModel.fetchTagRecipes()
            local -> viewModel.getLocalRecipes()
            else -> viewModel.fetchRecipes()
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = RecipeAdapter(local)
        recipe_list.adapter = adapter
        subscribeUi(adapter)

        fab_create_recipe.setOnClickListener {
            val direction = RecipeListFragmentDirections.actionRecipeListFragmentToRecipeCreateFragment()
            it.findNavController().navigate(direction)
        }
    }

    private fun subscribeUi(adapter: RecipeAdapter) {
        viewModel.resource.observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    hideLoadingBar()
                    if (resource.data.isEmpty()) {
                        view?.snack("There are no recipes")
                        findNavController().navigateUp()
                    } else {
                        adapter.submitList(resource.data)
                    }
                }
                Status.ERROR -> {
                    hideLoadingBar()
                    showError(resource.errorMessage)
                }
                Status.LOADING -> showLoadingBar()
            }
        })
    }
}
