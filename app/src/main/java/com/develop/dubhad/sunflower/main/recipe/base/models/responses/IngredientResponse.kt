package com.develop.dubhad.sunflower.main.recipe.base.models.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class IngredientResponse(
    @Json(name = "ingredient_id") val ingredientId: Int?,
    @Json(name = "quantity") val quantity: Float?,
    @Json(name = "measure") val weightMeasure: String?,
    @Json(name = "product") val product: String?
)
