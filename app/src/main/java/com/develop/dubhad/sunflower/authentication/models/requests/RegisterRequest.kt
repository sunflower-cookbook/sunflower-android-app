package com.develop.dubhad.sunflower.authentication.models.requests

import com.develop.dubhad.sunflower.authentication.models.domain.RegistrationUser
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RegisterRequest(
    @Json(name = "username") val username: String,
    @Json(name = "email") val email: String,
    @Json(name = "password1") val password: String,
    @Json(name = "password2") val repeatedPassword: String
) {
    constructor(user: RegistrationUser) : this(
        username = user.username,
        email = user.email,
        password = user.password,
        repeatedPassword = user.repeatedPassword
    )
}
