package com.develop.dubhad.sunflower.base.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.develop.dubhad.sunflower.authentication.viewmodels.LogInViewModel
import com.develop.dubhad.sunflower.authentication.viewmodels.RegisterViewModel
import com.develop.dubhad.sunflower.base.viewmodels.ViewModelFactory
import com.develop.dubhad.sunflower.base.viewmodels.ViewModelKey
import com.develop.dubhad.sunflower.main.cookbook.viewmodels.CookbookListViewModel
import com.develop.dubhad.sunflower.main.recipe.create.viewmodels.RecipeCreateViewModel
import com.develop.dubhad.sunflower.main.recipe.detail.viewmodels.RecipeDetailViewModel
import com.develop.dubhad.sunflower.main.recipe.list.viewmodels.RecipeListViewModel
import com.develop.dubhad.sunflower.main.tag.viewmodels.TagListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LogInViewModel::class)
    abstract fun bindUserViewModel(logInViewModel: LogInViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    abstract fun bindRegisterViewModel(registerViewModel: RegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CookbookListViewModel::class)
    abstract fun bindCookbookListViewModel(cookbookListViewModel: CookbookListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecipeListViewModel::class)
    abstract fun bindRecipeListViewModel(recipeListViewModel: RecipeListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecipeDetailViewModel::class)
    abstract fun bindRecipeDetailViewModel(recipeDetailViewModel: RecipeDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TagListViewModel::class)
    abstract fun bindTagListViewModel(tagListViewModel: TagListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecipeCreateViewModel::class)
    abstract fun bindRecipeCreateViewModel(recipeCreateViewModel: RecipeCreateViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
