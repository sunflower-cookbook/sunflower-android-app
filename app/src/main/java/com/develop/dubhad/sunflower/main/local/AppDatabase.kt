package com.develop.dubhad.sunflower.main.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.develop.dubhad.sunflower.main.local.dao.AuthorDao
import com.develop.dubhad.sunflower.main.local.dao.RecipeDao
import com.develop.dubhad.sunflower.main.local.models.AuthorEntity
import com.develop.dubhad.sunflower.main.local.models.IngredientEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeStepEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeTagCrossRef
import com.develop.dubhad.sunflower.main.local.models.TagEntity

@Database(
    entities = [
        AuthorEntity::class,
        IngredientEntity::class,
        RecipeEntity::class,
        RecipeStepEntity::class,
        RecipeTagCrossRef::class,
        TagEntity::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun authorDao(): AuthorDao

    abstract fun recipeDao(): RecipeDao
}
