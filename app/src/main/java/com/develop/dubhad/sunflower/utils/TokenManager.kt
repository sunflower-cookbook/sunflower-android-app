package com.develop.dubhad.sunflower.utils

import android.content.SharedPreferences
import dagger.Reusable
import javax.inject.Inject

@Reusable
class TokenManager @Inject constructor(private val sharedPrefs: SharedPreferences) {

    companion object {
        private const val PREF_TOKEN = "PREF_TOKEN"
    }

    fun saveToken(token: String) {
        sharedPrefs.edit()
            .putString(PREF_TOKEN, token)
            .apply()
    }

    fun getToken(): String {
        return "Token ${getRawToken()}"
    }

    fun isTokenExists(): Boolean {
        return sharedPrefs.contains(PREF_TOKEN)
    }

    fun clearToken() {
        sharedPrefs.edit()
            .remove(PREF_TOKEN)
            .apply()
    }

    private fun getRawToken(): String {
        return sharedPrefs.getString(PREF_TOKEN, "") ?: ""
    }
}
